//
//  JSQMessageAttributedData.h
//  Pods
//
//  Created by Vishal Gabani on 10/03/16.
//
//

#import "JSQMessageMediaData.h"

@protocol JSQMessageAttributedData <JSQMessageData>

/**
 *  @return The attributed text of the message.
 *
 *  @warning You must not return 'nil' from this method.
 */

- (NSAttributedString *)attributedText;

@end